
local class = require "class"
local Entity = require "physics.Entity"
local Vec2 = require "physics.Vec2"
local CircleShape = require "physics.CircleShape"
local Body = require "physics.Body"

require "globals"

local PROJECTILE_SPEED = 500
local PROJECTILE_MIN_SPEED = 200
local PROJECTILE_COLOUR = { 128, 128, 128 }

local Projectile = class( Entity )

function Projectile:new( x, y, xd, yd, owner, damage )
	self:super( Vec2( x, y ), CircleShape( PROJECTILE_SIZE ), Body.Dynamic )
	self.velocity = Vec2( xd, yd ) * PROJECTILE_SPEED
	self.owner = owner
	self.damage = damage
end

function Projectile:update( dt )
	if self.velocity:len() < PROJECTILE_MIN_SPEED then
		self:remove()
	end
end

function Projectile:draw()
	love.graphics.setColor( PROJECTILE_COLOUR )
	love.graphics.circle( "fill", self.position.x, self.position.y, PROJECTILE_SIZE )
end

function Projectile:filter()
	return false
end

return Projectile
