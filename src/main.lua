
local timer = require "timer"
local config = require "config"
local coroutines = require "coroutines"

if love.filesystem.exists "config.txt" then
	GLOBAL_CONFIG = assert( config.open "config.txt" )
else
	GLOBAL_CONFIG = assert( config.open "res/default_config.txt" )
	GLOBAL_CONFIG.path = "config.txt"
end

GLOBAL_CONFIG:autosave()

local state = require "ui.state"
local menu_state = require "ui.states.menu_state"
local game_state = require "ui.states.game_state"
local new_state = require "ui.states.new_state"

require "powerups" .init()

local font

math.randomseed( os.time() )

local WINDOW_ICON = love.image.newImageData( GLOBAL_CONFIG:read "window.ICON_PATH" )

local function get_key_modifier()
	return ((love.keyboard.isDown "lctrl" or love.keyboard.isDown "rctrl") and "ctrl-" or "")
	    .. ((love.keyboard.isDown "lshift" or love.keyboard.isDown "rshift") and "shift-" or "")
	    .. ((love.keyboard.isDown "lalt" or love.keyboard.isDown "ralt") and "alt-" or "")
end

function love.load()
	font = love.graphics.getFont()

	love.graphics.setBackgroundColor( require "ui.theme" .get_background_colour() )

	love.window.setTitle( GLOBAL_CONFIG:read "window.TITLE" )
	love.window.setMode( GLOBAL_CONFIG:read "window.WIDTH", GLOBAL_CONFIG:read "window.HEIGHT", {} )
	love.window.setIcon( WINDOW_ICON )

	state.set( menu_state )
end

function love.draw()
	local fps = tostring( love.timer.getFPS() )
	local window_width = love.window.getMode()

	if state.get().draw then
		state.get():draw()
	end

	love.graphics.setColor( 0, 0, 0 )
	love.graphics.setFont( font )
	love.graphics.print( fps, window_width - font:getWidth( fps ), 0 )
end

function love.update( dt )
	if state.get().update then
		state.get():update( dt )
	end
	timer.update( dt )
	coroutines.update()
end

function love.mousepressed( x, y, button, is_touch )
	if not is_touch and state.get().mousepressed then
		state.get():mousepressed( x, y, button, get_key_modifier() )
	end
end

function love.mousereleased( x, y, button, is_touch )
	if not is_touch and state.get().mousereleased then
		state.get():mousereleased( x, y, button, get_key_modifier() )
	end
end

function love.mousemoved( x, y, dx, dy, is_touch )
	if not is_touch and state.get().mousemoved then
		state.get():mousemoved( x, y, dx, dy, get_key_modifier() )
	end
end

function love.touchpressed( ID, x, y )
	if state.get().touchpressed then
		state.get():touchpressed( ID, x, y, get_key_modifier() )
	end
end

function love.touchreleased( ID, x, y )
	if state.get().touchreleased then
		state.get():touchreleased( ID, x, y, get_key_modifier() )
	end
end

function love.touchmoved( ID, x, y, dx, dy )
	if state.get().touchmoved then
		state.get():touchmoved( ID, x, y, dx, dy, get_key_modifier() )
	end
end

function love.keypressed( key )
	if state.get().keypressed then
		state.get():keypressed( key, get_key_modifier() )
	end
end

function love.keyreleased( key )
	if state.get().keyreleased then
		state.get():keyreleased( key, get_key_modifier() )
	end
end

function love.quit()
	if GLOBAL_CONFIG:read "debug_mode" then
		love.filesystem.remove "config.txt"
	end
end
