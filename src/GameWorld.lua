
local class = require "class"
local World = require "physics.World"
local Cell = require "Cell"
local FoodSource = require "FoodSource"
local Projectile = require "Projectile"
local controller = require "controller"
local viewport = require "viewport"
local colours = require "ui.colours"
local Vec2 = require "physics.Vec2"
local theme = require "ui.theme"
local powerups = require "powerups"
local Powerup = require "Powerup"

require "globals"

local MIN_FOOD_SIZE = 4
local MAX_FOOD_SIZE = 8
local MIN_CELL_SIZE = 25
local MAX_CELL_SIZE = 80
local MOVEMENT_FORCE = 2
local FOOD_SPAWN_RATE = 0.01
local POWERUP_SPAWN_RATE = 0.002
local GRID_SHADER_CODE = love.filesystem.read "res/shaders/grid.glsl"
local EDGE_SHADER_CODE = love.filesystem.read "res/shaders/boundary.glsl"
local SCALE_FACTOR = 0.1
local SCORE_FONT = love.graphics.newFont( "res/architects_daughter_font/ArchitectsDaughter.ttf", 50 )

local blank_image

do
	local image_data = love.image.newImageData( 1, 1 )
	image_data:setPixel( 0, 0, 255, 255, 255, 255 )
	blank_image = love.graphics.newImage( image_data )
end

local GameWorld = class( World )

local grid_shader, edge_shader

GameWorld.viewport = nil

function GameWorld:new( map_size )
	local cw, ch = 0, 0

	if map_size == MAP_SIZE_DEBUG then
		cw, ch = 12, 8
	elseif map_size == MAP_SIZE_SMALL then
		cw, ch = 40, 32
	elseif map_size == MAP_SIZE_MEDIUM then
		cw, ch = 60, 48
	elseif map_size == MAP_SIZE_LARGE then
		cw, ch = 80, 64
	else
		return error( "Invalid world size " .. tostring( map_size ), 2 )
	end

	self.scale = 1
	self.player_max_size = 0
	self.player_time_alive = 0
	self.time = 0
	self.eating = true
	self.player_cells = {}
	self.food_spawn_timer = 0
	self.powerup_spawn_timer = 0
	self.viewport = viewport( w, h )
	self.spawn_artifacts = true
	self:super( cw, ch, CHUNK_SIZE )

	self.viewport.x, self.viewport.y = self:get_centre()

	grid_shader = love.graphics.newShader( GRID_SHADER_CODE )
	edge_shader = love.graphics.newShader( EDGE_SHADER_CODE )
	grid_shader:send( "grid_colour", theme.get_grid_colour() )
	edge_shader:send( "world_top_left", { 0, 0 } )
	edge_shader:send( "world_bottom_right", { self.chunknh * self.chunk_size, self.chunknv * self.chunk_size } )
end

function GameWorld:get_bounds()
	return 0, 0, self.chunknh * self.chunk_size - 1, self.chunknv * self.chunk_size - 1
end

function GameWorld:get_centre()
	return self.chunknh * self.chunk_size / 2, self.chunknv * self.chunk_size / 2
end

function GameWorld:spawn_food_in_area( count, x1, y1, x2, y2 )
	local t = {}

	for i = 1, count do
		local size = math.random( MIN_FOOD_SIZE, MAX_FOOD_SIZE )
		local x = math.random( x1 + size, x2 - size )
		local y = math.random( y1 + size, y2 - size )

		t[i] = self:add_entity( FoodSource( x, y, size ) )
	end

	return t
end

function GameWorld:spawn_cells_in_area( count, x1, y1, x2, y2 )
	local t = {}

	for i = 1, count do
		local size = math.random( MIN_CELL_SIZE, MAX_CELL_SIZE )
		local x = math.random( x1 + size, x2 - size )
		local y = math.random( y1 + size, y2 - size )
		local colour_name = colours.random_name()
		local colour_light = colours.light_from_name( colour_name )
		local colour_dark  = colours. dark_from_name( colour_name )

		t[i] = self:add_entity( Cell( x, y, size, colour_light, colour_dark ) )
	end

	return t
end

function GameWorld:get_chunk_bounds( cxmin, cymin, cxmax, cymax )
	local csize = self.chunk_size

	cxmax = (cxmax or cxmin) + 1
	cymax = (cymax or cymin) + 1

	return cxmin * csize, cymin * csize, cxmax * csize - 1, cymax * csize - 1
end

function GameWorld:register_player_cell( cell )
	self.player_cells[#self.player_cells + 1] = cell
	cell.controller = controller.player()
end

function GameWorld:spawn_powerups_in_area( count, x1, y1, x2, y2 )
	for i = 1, count or 1 do
		local x = math.random( x1 + POWERUP_SIZE, x2 - POWERUP_SIZE )
		local y = math.random( y1 + POWERUP_SIZE, y2 - POWERUP_SIZE )
		local random_powerup = powerups.random_name()

		self:add_entity( Powerup( x, y, random_powerup, math.random( 20, 30 ) ) )
	end
end

function GameWorld:spawn_projectile( cell, xd, yd, damage )
	local x = cell.position.x + (cell.shape.radius + 5 + PROJECTILE_SIZE) * xd
	local y = cell.position.y + (cell.shape.radius + 5 + PROJECTILE_SIZE) * yd

	self:add_entity( Projectile( x, y, xd, yd, cell, damage ) ).damping = 0.1
end

function GameWorld:update( dt )
	local xmin, ymin, xmax, ymax = self:get_player_bounds()
	local size = math.sqrt( (xmax - xmin) * (ymax - ymin) ) / 100
	local xmin, ymin, xmax, ymax = self:get_bounds()
	local world_area = (xmax - xmin) * (ymax - ymin)

	local food_spawn_rate = self.chunk_size ^ 2 / (FOOD_SPAWN_RATE * world_area)
	local powerup_spawn_rate = 1 / (POWERUP_SPAWN_RATE * world_area)

	if #self.player_cells > 0 then
		self.viewport.target_zoom = self.scale / size
		self.viewport.x, self.viewport.y = self:get_player_centre()
	end

	for i = #self.entities, 1, -1 do
		local entity = self.entities[i]

		if entity.class == Cell then
			local xd, yd, speed = entity:controller( self )

			if entity.powerups.speed_boost.active then
				speed = speed * 2
			end

			entity:apply_force( Vec2( xd, yd ) * speed * dt * MOVEMENT_FORCE )
		end

		if entity.update then
			entity:update( dt )
		end
	end

	World.update( self, dt )

	self.player_max_size = math.max( self.player_max_size, self:get_cell_size( self.player_cells ) )

	self.player_time_alive = self.player_time_alive + dt
	self.time = self.time + dt
	self.food_spawn_timer = self.food_spawn_timer + dt
	self.powerup_spawn_timer = self.powerup_spawn_timer + dt

	while self.food_spawn_timer > food_spawn_rate and self.spawn_artifacts do
		self.food_spawn_timer = self.food_spawn_timer - food_spawn_rate

		self:spawn_food_in_area( 1, self:get_bounds() )
	end

	while self.powerup_spawn_timer > powerup_spawn_rate and self.spawn_artifacts do
		self.powerup_spawn_timer = self.powerup_spawn_timer - powerup_spawn_rate
	end

	for i = #self.player_cells, 1, -1 do
		if not self.player_cells[i].world then
			table.remove( self.player_cells, i )
		end
	end
end

function GameWorld:get_player_centre()
	local x, y = 0, 0
	local s = 0

	for i = 1, #self.player_cells do
		local r = self.player_cells[i].shape.radius ^ 2
		x = x + self.player_cells[i].position.x * r
		y = y + self.player_cells[i].position.y * r
		s = s + r
	end

	return x / s, y / s
end

function GameWorld:get_player_bounds()
	local xmin, ymin, xmax, ymax = math.huge, math.huge, -math.huge, -math.huge

	for i = 1, #self.player_cells do
		local x, y, r = self.player_cells[i].position.x, self.player_cells[i].position.y, self.player_cells[i].shape.radius
		xmin = math.min( xmin, x - r )
		xmax = math.max( xmax, x + r )
		ymin = math.min( ymin, y - r )
		ymax = math.max( ymax, y + r )
	end

	return xmin, ymin, xmax, ymax
end

function GameWorld:get_cell_size( cells )
	local s = 0

	for i = 1, #cells do
		s = s + cells[i].shape.radius ^ 2
	end

	return s * math.pi
end

function GameWorld:draw()
	local ucx, ucy = self:get_player_centre()
	local viewport = self.viewport
	local draw_offset_x = viewport.width / 2 - ucx * viewport.zoom
	local draw_offset_y = viewport.height / 2 - ucy * viewport.zoom

	-- send grid/boundary shader variables
	grid_shader:send( "draw_offset", { -draw_offset_x, -draw_offset_y } )
	grid_shader:send( "draw_scale", viewport.zoom )

	edge_shader:send( "draw_offset", { -draw_offset_x, -draw_offset_y } )
	edge_shader:send( "draw_scale", viewport.zoom )

	-- draw the grid and boundary
	love.graphics.setShader( grid_shader )
	love.graphics.draw( blank_image, 0, 0, 0, viewport.width, viewport.height )
	love.graphics.setShader( edge_shader )
	love.graphics.draw( blank_image, 0, 0, 0, viewport.width, viewport.height )
	love.graphics.setShader()

	local vxmin, vymin, vxmax, vymax = viewport:get_bounds()

	viewport:apply_love2d_transformation()

	for i = 1, #self.entities do
		self.entities[i]:draw()
	end

	love.graphics.pop()

	if #self.player_cells > 0 then
		love.graphics.setFont( SCORE_FONT )
		love.graphics.setColor( theme.get_text_colour() )
		love.graphics.print( self:get_player_score(), 0, 0 )
	end
end

function GameWorld:get_player_score()
	return math.floor( (100 * self.player_time_alive + (self.player_max_size - INITIAL_PLAYER_SIZE)) / 100 + 0.5 )
end

function GameWorld:set_scale( scale )
	self.scale = scale
end

function GameWorld:zoom_in( n )
	self.scale = self.scale * (1 + SCALE_FACTOR * (n or 1))
	self.viewport.zoom = self.viewport.zoom * (1 + SCALE_FACTOR * (n or 1))
end

function GameWorld:zoom_out( n )
	self.scale = self.scale / (1 + SCALE_FACTOR * (n or 1))
	self.viewport.zoom = self.viewport.zoom / (1 + SCALE_FACTOR * (n or 1))
end

return GameWorld
