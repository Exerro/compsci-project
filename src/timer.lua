
local timer = {}
local timers = {}

function timer.update( dt )
	for i = #timers, 1, -1 do
		timers[i].timeout = timers[i].timeout - dt

		if timers[i].timeout <= 0 then
			table.remove( timers, i ).callback()
		end
	end
end

function timer.queue( timeout, callback )
	timers[#timers + 1] = { timeout = timeout, callback = callback }
end

return timer
