
local util = {}

function util.test_point_box( x, y, x1, y1, w1, h1 )
	return x >= x1 and x < x1 + w1 and y > y1 and y < y1 + h1
end

function util.test_bounding_box( x1, y1, w1, h1, x2, y2, w2, h2 )
	return x1 + w1 >= x2 and x2 + w2 >= x2 and y1 + h1 >= y2 and y2 + h2 >= y1
end

function util.sigmoid( n )
	return 1 / (1 + math.exp( -n ))
end

function util.count_keys( t )
	local n = 0
	for k, v in pairs( t ) do
		n = n + 1
	end
	return n
end

return util
