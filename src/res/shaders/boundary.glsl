
extern vec2 draw_offset;
extern float draw_scale;
extern float time;
extern vec2 world_top_left;
extern vec2 world_bottom_right;

vec4 effect(vec4 colour, Image texture, vec2 image_coords, vec2 screen_coords) {
	vec2 world_coords = (screen_coords + draw_offset) / draw_scale;
	bool coloured = mod( world_coords.x + world_coords.y + time * 40, 50.0 ) < 25.0;

	if (world_coords.x < world_top_left.x
	||  world_coords.y < world_top_left.y
	||  world_coords.x > world_bottom_right.x
	||  world_coords.y > world_bottom_right.y) {
		return coloured ? vec4( 1.0, 0.1, 0.2, 0.4 ) : vec4( 0.0, 0.0, 0.0, 0.0 );
	}
	else {
		return vec4( 0.0, 0.0, 0.0, 0.0 );
	}
}
