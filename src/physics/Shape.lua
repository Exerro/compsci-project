
local class = require "class"

local Shape = class()

function Shape:get_area()
	return 0
end

function Shape:get_bounds()
	return 0, 0, 0, 0
end

return Shape
