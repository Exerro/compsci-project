
local class = require "class"
local Vec2 = require "physics.Vec2"

local Entity = class()

function Entity:new( position, shape, body )
	self.position = position
	self.shape = shape
	self.body = body
	self.velocity = Vec2.zero
	self.moved = false
	self.damping = 1
end

function Entity:get_mass()
	return self.shape:get_area()
end

function Entity:apply_force( force )
	self.velocity = self.velocity:add( force )
end

function Entity:on_collision( entity )
	return true
end

function Entity:filter( entity )
	return true
end

function Entity:remove()
	if self.world then
		return self.world:remove_entity( self )
	end
end

return Entity
