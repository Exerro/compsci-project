
local class = require "class"
local Vec2 = require "physics.Vec2"
local CircleShape = require "physics.CircleShape"
local RectangleShape = require "physics.RectangleShape"

local Collision = class()

function Collision:new( entity1, entity2, penetration, normal )
	self.entity1 = entity1
	self.entity2 = entity2
	self.penetration = penetration
	self.normal = normal
end

function Collision.test( e1, e2 )
	if e1.shape.class == CircleShape then
		if e2.shape.class == CircleShape then
			local dx = e1.position.x - e2.position.x
			local dy = e1.position.y - e2.position.y
			local dist2 = dx * dx + dy * dy
			local radius = e1.shape.radius + e2.shape.radius

			if dist2 < radius * radius then
				local dist = math.sqrt( dist2 )
				return Collision( e1, e2, radius - dist, Vec2( dx / dist, dy / dist) )
			end

			return nil
		elseif e2.shape.class == RectangleShape then
			if e1.position.x >= e2.position.x and e1.position.x < e2.position.x + e2.shape.width then -- top or bottom collision test
				local top_penetration = e1.position.y + e1.shape.radius - e2.position.y
				local bottom_penetration = e2.position.y + e2.shape.height + e1.shape.radius - e1.position.y

				if top_penetration >= 0 and top_penetration <= math.max( e1.shape.radius, e2.shape.height / 2 ) then
					return Collision( e1, e2, top_penetration, Vec2( 0, -1 ) ) -- top collision
				elseif bottom_penetration >= 0 and bottom_penetration <= math.max( e1.shape.radius, e2.shape.height / 2 ) then -- bottom collision
				 	return Collision( e1, e2, bottom_penetration, Vec2( 0, 1 ) )
				end
			end

			if e1.position.y >= e2.position.y and e1.position.y < e2.position.y + e2.shape.height then -- left or right collision test
				local left_penetration = e1.position.x + e1.shape.radius - e2.position.x
				local right_penetration = e2.position.x + e2.shape.width + e1.shape.radius - e1.position.x

				if left_penetration >= 0 and left_penetration <= math.max( e1.shape.radius, e2.shape.width / 2 ) then -- left collision
					return Collision( e1, e2, left_penetration, Vec2( -1, 0 ) )
				elseif right_penetration >= 0 and right_penetration <= math.max( e1.shape.radius, e2.shape.width / 2 ) then -- right collision
				 	return Collision( e1, e2, right_penetration, Vec2( 1, 0 ) )
				end
			end

			local pen, dist, v

			v = e1.position:sub( e2.position )
			dist = v:len()
			pen = e1.shape.radius - dist

			if pen > 0 then -- top left corner
				return Collision( e1, e2, pen, v:div( dist ) )
			end

			v = e1.position:sub( e2.position ):sub( Vec2( e2.shape.width, 0 ) )
			dist = v:len()
			pen = e1.shape.radius - dist

			if pen > 0 then -- top right corner
				return Collision( e1, e2, pen, v:div( dist ) )
			end

			v = e1.position:sub( e2.position ):sub( Vec2( e2.shape.width, e2.shape.height ) )
			dist = v:len()
			pen = e1.shape.radius - dist

			if pen > 0 then -- bottom right corner
				return Collision( e1, e2, pen, v:div( dist ) )
			end

			v = e1.position:sub( e2.position ):sub( Vec2( 0, e2.shape.height ) )
			dist = v:len()
			pen = e1.shape.radius - dist

			if pen > 0 then -- bottom left corner
				return Collision( e1, e2, pen, v:div( dist ) )
			end

			return nil
		end
	elseif e1.shape.class == RectangleShape then
		if e2.shape.class == CircleShape then
			return Collision.test( e2, e1 )
		elseif e2.shape.class == RectangleShape then
			error "Attempted rectangle-rectangle collision test"
		end
	end
end

return Collision
