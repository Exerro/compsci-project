
local class = require "class"
local Vec2 = require "physics.Vec2"
local Body = require "physics.Body"
local Collision = require "physics.Collision"

local max = math.max
local min = math.min
local abs = math.abs
local floor = math.floor

local World = class()

function World:new( ch, cv, chunk_size )
	self.chunknh = ch
	self.chunknv = cv
	self.chunks = {}
	self.entities = {}
	self.chunk_size = chunk_size or 64
	self.entity_chunks = {}
	self.non_convergent_collisions = true

	for i = 1, ch * cv do
		self.chunks[i] = {}
	end
end

function World:clip_entity( entity )
	local wwidth = self.chunk_size * self.chunknh - 1
	local wheight = self.chunk_size * self.chunknv - 1
	local bxmin, bymin, bxmax, bymax = entity.shape:get_bounds()
	local ex, ey = entity.position.x, entity.position.y
	local exmin = ex + bxmin
	local eymin = ey + bymin
	local exmax = ex + bxmax
	local eymax = ey + bymax
	local changed = false

	if exmin < 0 then
		ex = ex - exmin
		changed = true
	end

	if eymin < 0 then
		ey = ey - eymin
		changed = true
	end

	if exmax > wwidth then
		ex = ex + wwidth - exmax
		changed = true
	end

	if eymax > wheight then
		ey = ey + wheight - eymax
		changed = true
	end

	if changed then
		entity:on_collision() -- tell the entity it's collided with an edge
		entity.position = Vec2( ex, ey )
	end
end

function World:get_chunk( x, y )
	return y * self.chunknh + x + 1
end

function World:world_to_chunk_coords( v )
	return v:div( self.chunk_size )
end

function World:get_chunk_intersections( e )
	local bxmin, bymin, bxmax, bymax = e.shape:get_bounds()
	local w, h = self.chunknh - 1, self.chunknv - 1
	local pos = e.position
	local top_left = self:world_to_chunk_coords( Vec2( pos.x + bxmin, pos.y + bymin ) )
	local bottom_right = self:world_to_chunk_coords( Vec2( pos.x + bxmax, pos.y + bymax ) )
	local res = {}
	local i = 1

	local xmin, ymin = floor( top_left.x ), floor( top_left.y )
	local xmax, ymax = floor( bottom_right.x ), floor( bottom_right.y )

	if xmin < 0 then xmin = 0 end
	if ymin < 0 then ymin = 0 end
	if xmax > w then xmax = w - 1 end
	if ymax > h then ymax = h - 1 end

	local minchunki = ymin * self.chunknh + xmin + 1

	for y = ymin, ymax do
		for x = 0, xmax - xmin do
			res[i] = minchunki + x
			i = i + 1
		end
		minchunki = minchunki + self.chunknh
	end

	return res
end

function World:add_entity( e )
	self.entities[#self.entities + 1] = e
	self:clip_entity( e )

	local intersections = self:get_chunk_intersections( e )

	for i = 1, #intersections do
		self:add_entity_to_chunk( e, intersections[i] )
	end

	self.entity_chunks[e] = intersections
	e.world = self

	return e
end

function World:remove_entity( e )
	local intersections = self:get_chunk_intersections( e )

	for i = 1, #intersections do
		self:remove_entity_from_chunk( e, intersections[i] )
	end

	self.entity_chunks[e] = nil
	e.world = nil

	for i = #self.entities, 1, -1 do
		if self.entities[i] == e then
			return table.remove( self.entities, i )
		end
	end
end

function World:add_entity_to_chunk( e, chunk )
	local c = self.chunks[chunk]
	c[#c + 1] = e
end

function World:remove_entity_from_chunk( e, chunk )
	local c = self.chunks[chunk]

	for i = 1, #c do
		if c[i] == e then
			return table.remove( c, i )
		end
	end
end

function World:update_entity_chunks( cache )
	for i = 1, #self.entities do
		local entity = self.entities[i]
		if entity.moved then
			local chunk_list = self:get_chunk_intersections( entity )
			local cachee = cache[entity]
			local i1, i2 = 1, 1

			while i1 <= #chunk_list and i2 <= #cachee do
				if chunk_list[i1] == cachee[i2] then
					i1 = i1 + 1
					i2 = i2 + 1
				elseif chunk_list[i1] > cachee[i2] then
					self:remove_entity_from_chunk( entity, cachee[i2] )
					i2 = i2 + 1
				elseif chunk_list[i1] < cachee[i2] then
					self:add_entity_to_chunk( entity, chunk_list[i1] )
					i1 = i1 + 1
				end
			end

			while i1 <= #chunk_list do
				self:add_entity_to_chunk( entity, chunk_list[i1] )
				i1 = i1 + 1
			end

			while i2 <= #cachee do
				self:remove_entity_from_chunk( entity, cachee[i2] )
				i2 = i2 + 1
			end

			cache[entity] = chunk_list
			entity.moved = false
		end
	end
end

function World:update( dt )
	local cache = self.entity_chunks

	for i = #self.entities, 1, -1 do
		local entity = self.entities[i]
		local vs = 1 - entity.damping * dt

		if entity.body == Body.Dynamic then -- note, projectiles may need custom movement to test collisions on the way
			local dx = entity.velocity.x
			local dy = entity.velocity.y

			entity.position = entity.position:add( Vec2( dx * dt, dy * dt ) )
			entity.velocity = Vec2( dx * vs, dy * vs )

			if not (dx > -0.01 and dx < 0.01 and dy > -0.01 and dy < 0.01) then
				self:clip_entity( entity )
				entity.moved = true
			end
		end
	end

	self:update_entity_chunks( cache )
	self:resolve_collisions()
	self:update_entity_chunks( cache )
end

function World:resolve_collisions( dt )
	local collisions_found = {}
	local collisions = {}
	local collision
	local i = 1
	local test = Collision.test

	for c = 1, #self.chunks do -- iterate through all chunks
		local chunk = self.chunks[c]
		for e1 = 1, #chunk do -- iterate through all entities in the chunk
			local entity1 = chunk[e1]
			if not collisions_found[entity1] then collisions_found[entity1] = {} end
			for e2 = e1 + 1, #chunk do -- iterate through all other entities in the chunk
				local entity2 = chunk[e2]
				if entity1 ~= entity2 and not collisions_found[entity1][entity2] then
					if entity1.body == Body.Dynamic or entity2.body == Body.Dynamic then -- if one of the entities are dynamic
						collision = test( entity1, entity2 ) -- get the collision
						if collision then -- if a collision was found
							if not collisions_found[entity2] then collisions_found[entity2] = {} end

							collisions_found[entity1][entity2] = true
							collisions_found[entity2][entity1] = true
							collisions[i] = collision
							i = i + 1
						end
					end
				end
			end
		end
	end

	for c = 1, #collisions do
		local e1, e2 = collisions[c].entity1, collisions[c].entity2
		local f1, f2 = e1:filter( e2 ), e2:filter( e1 )

		e1:on_collision( e2, collisions[c] )
		e2:on_collision( e1, collisions[c] )

		if f1 or f2 then
			self:resolve_collision( collisions[c] )
		end
	end
end

function World:resolve_collision( collision )
	local entity1 = collision.entity1
	local entity2 = collision.entity2
	local mass1 = entity1:get_mass()
	local mass2 = entity2:get_mass()
	local restitution = 0.8 -- coef of restitution
	local static_body = Body.Static
	local normal = collision.normal
	local penetration = collision.penetration

	if entity2.body == static_body then
		entity1, entity2 = entity2, entity1
		normal = -normal
	end

	-- resolve position instantaneously
	if (entity1.body == Body.Static) then
		entity2.position = entity2.position - normal * penetration

		self:clip_entity( entity2 )
		entity2.moved = true
	else
		local masst = mass1 + mass2
		local res1 = mass2 / masst -- scaling factor of effect on each entity's instantaneous movement
		local res2 = mass1 / masst
		local nx, ny = normal.x, normal.y
		local rs1, rs2 = penetration * res1, penetration * res2

		entity1.position = entity1.position + Vec2( nx * rs1, ny * rs1 ) -- entity1.position:add( normal:mul( collision.penetration * res1 ) )
		entity2.position = entity2.position - Vec2( nx * rs2, ny * rs2 ) -- entity2.position:sub( normal:mul( collision.penetration * res2 ) )

		self:clip_entity( entity1 )
		self:clip_entity( entity2 )

		entity1.moved = true
		entity2.moved = true
	end

	-- apply restitution
	if (entity1.body == Body.Static) then
		entity2.velocity = entity2.velocity:sub( normal:mul( velocity2:dot( normal ) * (1 + restitution) ) )
	else
		local e1v, e2v = velocity1, velocity2
		local u1 = entity1.velocity:dot( normal ) -- initial speed of e1 -> e2
		local u2 = entity2.velocity:dot( normal ) -- initial speed of e2 -> e1
		local im = u1*mass1 + u2*mass2 -- initial momentum
		local app = u2 - u1 -- speed of approach
		-- e = sep / app => sep = e * app (where e is restitution)
		local sep = restitution * app
		-- (1) sep = v1 - v2 => v1 = sep + v2
		-- assuming elastic collision => conservation of momentum
		-- (2) m1*v1 + m2*v2 = m1*u1 + m2*u2 = im
		-- substituting (1) into (2)
		-- (3) m1*sep + m1*v2 + m2*v2 = im
		-- ... v2(m2 + m1) = im - m1*sep
		-- ... v2 = (im - m1*sep) / (m2 + m1)
		local v2 = (im - mass1*sep) / (mass2 + mass1)
		local v1 = sep + v2
		local d1 = u1 - v1
		local d2 = u2 - v2

		entity1.velocity = entity1.velocity - Vec2( normal.x * d1, normal.y * d1 ) -- entity1.velocity:sub( normal:mul( u1 - v1 ) )
		entity2.velocity = entity2.velocity - Vec2( normal.x * d2, normal.y * d2 ) -- entity2.velocity:sub( normal:mul( u2 - v2 ) )
	end
end

return World
