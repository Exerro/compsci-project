
local event_methods = {}
local event_mt = { __index = event_methods }

function event_methods:new( name, parameters )
	self.name = name
	self.parameters = parameters

	self.handled = false
	self.handler = nil
end

function event_methods:handle( handler )
	self.handled = true
	self.handler = handler
end

function event_methods:child( parameters )
	local obj = setmetatable( {}, event_mt )
	local param = {}

	for k, v in pairs( self.parameters ) do
		param[k] = v
	end

	for k, v in pairs( parameters or {} ) do
		param[k] = v
	end

	obj:new( self.name, param )

	obj.handled = self.handled
	obj.handler = self.handler

	function obj.handle( obj, handler )
		obj.handled = true
		obj.handler = handler

		return self:handle( handler )
	end

	return obj
end

return function( ... )
	local obj = setmetatable( {}, event_mt )

	obj:new( ... )

	return obj
end
