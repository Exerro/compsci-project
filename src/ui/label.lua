
local class = require "class"
local label = class()

function label:new( x, y, w, h, t )
	self.x = x
	self.y = y
	self.width = w
	self.height = h
	self.text = t

	self.image = nil
	self.key_activator = nil
	self.colour = { 0, 0, 0, 0 }
	self.text_colour = { 0, 0, 0, 128 }
	self.font = love.graphics.newFont( 18 )
end

function label:draw()
	if self.image then
		love.graphics.setColor( self.colour )
		love.graphics.draw( self.image, self.x, self.y, 0, self.width / self.image:getWidth(), self.height / self.image:getHeight() )
	else
		love.graphics.setColor( self.colour )
		love.graphics.rectangle( "fill", self.x, self.y, self.width, self.height )
	end

	love.graphics.setFont( self.font )
	love.graphics.setColor( self.text_colour )
	love.graphics.print( self.text, self.x + self.width / 2 - self.font:getWidth( self.text ) / 2, self.y + self.height / 2 - self.font:getHeight() / 2 )
end

function label:update( dt )
	-- do nothing
end

function label:handle( event )
	-- do nothing
end

return label
