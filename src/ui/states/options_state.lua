
local button = require "ui.button"
local label = require "ui.label"
local event = require "ui.event"
local state = require "ui.state"
local theme = require "ui.theme"

local font = love.graphics.newFont( "res/architects_daughter_font/ArchitectsDaughter.ttf", 24 )
local font_large = love.graphics.newFont( "res/architects_daughter_font/ArchitectsDaughter.ttf", 64 )
local font_small = love.graphics.newFont( "res/architects_daughter_font/ArchitectsDaughter.ttf", 20 )
local button_image = love.graphics.newImage( "res/menu_button.png" )
local elements = {}

local function pass_event_to_elements( event, is_mouse_event )
	for i = #elements, 1, -1 do
		elements[i]:handle( is_mouse_event
			and event:child { x = event.parameters.x - elements[i].x, y = event.parameters.y - elements[i].y }
			or event )
	end
end

local options_state = {}

function options_state:load()
	local window_width, window_height = love.window.getMode()

	theme_selected = theme.get()

	elements[1] = button( window_width * 1/2 - 100, window_height - 110, 200, 60, "Back" )
	elements[2] = button( window_width / 2 - 150, window_height / 2 - 100, 40, 40, "" )
	elements[3] = button( window_width / 2 + 110, window_height / 2 - 100, 40, 40, "" )
	elements[4] =  label( window_width / 2 - 100, window_height / 2 - 140, 200, 50, "theme" )
	elements[5] =  label( window_width / 2 - 100, window_height / 2 - 100, 200, 50, theme_selected )

	elements[1].font = font
	elements[1].image = button_image
	elements[1].colour = { 255, 255, 255 }

	elements[4].font = font_small
	elements[5].font = font_small

	elements[4].text_colour = theme.get_text_colour()
	elements[5].text_colour = theme.get_text_colour()

	elements[2].image = love.graphics.newImage "res/arrow_left.png"
	elements[3].image = love.graphics.newImage "res/arrow_right.png"

	elements[1].key_activator = "backspace"

	elements[1].onClick = function( self )
		state.set( require "ui.states.menu_state" )
	end

	elements[2].onClick = function( self )
		theme_selected = theme_selected == "light" and "dark" or "light"
		elements[5].text = theme_selected
		elements[2].visible = theme_selected ~= "light"
		elements[3].visible = true

		theme.set( theme_selected )
		love.graphics.setBackgroundColor( theme.get_background_colour() )
		elements[4].text_colour = theme.get_text_colour()
		elements[5].text_colour = theme.get_text_colour()
	end

	elements[3].onClick = function( self )
		theme_selected = theme_selected == "dark" and "light" or "dark"
		elements[5].text = theme_selected
		elements[2].visible = true
		elements[3].visible = theme_selected ~= "dark"

		theme.set( theme_selected )
		love.graphics.setBackgroundColor( theme.get_background_colour() )
		elements[4].text_colour = theme.get_text_colour()
		elements[5].text_colour = theme.get_text_colour()
	end

	if theme_selected == "light" then
		elements[2].visible = false
	elseif theme_selected == "dark" then
		elements[3].visible = false
	end
end

function options_state:update( dt )
	for i = 1, #elements do
		elements[i]:update( dt )
	end
end

function options_state:mousepressed( x, y, button, modifier )
	pass_event_to_elements( event( "mouse_down", { x = x, y = y, button = button } ), true )
end

function options_state:mousereleased( x, y, button, modifier )
	pass_event_to_elements( event( "mouse_up", { x = x, y = y, button = button } ), true )
end

function options_state:touchpressed( ID, x, y, button, modifier )
	pass_event_to_elements( event( "touch_down", { ID = ID, x = x, y = y, button = button } ), true )
end

function options_state:touchreleased( ID, x, y, button, modifier )
	pass_event_to_elements( event( "touch_up", { ID = ID, x = x, y = y, button = button } ), true )
end

function options_state:keypressed( key, modifier )
	pass_event_to_elements( event( "key_down", { name = modifier .. key, raw = key, modifier = modifier } ), false )
end

function options_state:keyreleased( key, modifier )
	pass_event_to_elements( event( "key_up", { name = modifier .. key, raw = key, modifier = modifier } ), false )
end

function options_state:draw()
	for i = 1, #elements do
		elements[i]:draw()
	end

	love.graphics.setFont( font_large )
	love.graphics.setColor( theme.get_text_colour() )
	love.graphics.print( "Options", love.window.getMode() / 2 - font_large:getWidth "Options" / 2, 10 )
end

return options_state
